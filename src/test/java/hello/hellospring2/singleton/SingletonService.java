package hello.hellospring2.singleton;

public class SingletonService {

    // 1. 자기 자신을 내부 private로 static영역에 들고있음.
    private static final SingletonService instance = new SingletonService();


    // 2. public으로 열어서 객체 인스턴스가 필요하면 이 static 메서도를 통해서만 조회하도록 허용한다.
    public static SingletonService getInstance() {
        return instance;
    }

    // 생성자를 private로 선언해서 외부에서 new 키워드를 통해 객체를 생성하지 못하게 막는다.
    private SingletonService() {
    }

    public void logic() {
        System.out.println("싱글톤 객체 로직 호출");
    }



}
