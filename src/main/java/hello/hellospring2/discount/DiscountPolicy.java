package hello.hellospring2.discount;

import hello.hellospring2.member.Member;

public interface DiscountPolicy {

    /**
     * @return 할인 대상 금액
     */
    int discount(Member member, int price);



}
