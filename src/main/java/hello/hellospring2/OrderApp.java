package hello.hellospring2;

import hello.hellospring2.member.Grade;
import hello.hellospring2.member.Member;
import hello.hellospring2.member.MemberService;
import hello.hellospring2.member.MemberServiceImpl;
import hello.hellospring2.order.Order;
import hello.hellospring2.order.OrderService;
import hello.hellospring2.order.OrderServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class OrderApp {

    public static void main(String[] args) {
//        AppConfig appConfig = new AppConfig();
//        MemberService memberService = appConfig.memberService();
//        OrderService orderService = appConfig.orderService();

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);
        MemberService memberService = applicationContext.getBean("memberService", MemberService.class);
        OrderService orderService = applicationContext.getBean("orderService", OrderService.class);


        Long memberId = 1L;
        Member member = new Member(memberId, "memberA", Grade.VIP);
        memberService.join(member);

        Order order = orderService.createOrder(memberId, "itemA", 20000);

        System.out.println("order = " + order); //toString() 호출.
//        System.out.println("order = " + order.calculatePrice()); //toString() 호출.

    }
}
