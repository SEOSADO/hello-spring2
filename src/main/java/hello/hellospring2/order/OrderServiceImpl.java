package hello.hellospring2.order;

import hello.hellospring2.discount.DiscountPolicy;
import hello.hellospring2.discount.FixDiscountPolicy;
import hello.hellospring2.discount.RateDiscountPolicy;
import hello.hellospring2.member.Member;
import hello.hellospring2.member.MemberRepository;
import hello.hellospring2.member.MemoryMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderServiceImpl implements OrderService {

    private final MemberRepository memberRepository;
    private final DiscountPolicy discountPolicy;

    //    private final DiscountPolicy discountPolicy = new FixDiscountPolicy();  // DIP 위반, 항상 추상(Interface)에 의존해라
    //    private final DiscountPolicy discountPolicy = new RateDiscountPolicy();

    @Autowired
    public OrderServiceImpl(MemberRepository memberRepository, DiscountPolicy discountPolicy) {
        this.memberRepository = memberRepository;
        this.discountPolicy = discountPolicy;
    }

    @Override
    public Order createOrder(Long memberId, String itemName, int itemPrice) {
        // 단일 책임 원칙이 잘지켜진 예. 주문과 할인이 분리되어있음.
        Member member = memberRepository.findById(memberId);
        int discountPrice = discountPolicy.discount(member, itemPrice);

        return new Order(memberId, itemName, itemPrice, discountPrice);
    };

    // 테스트 용도
    public MemberRepository getMemberRepository() {
        return memberRepository;
    }
}
