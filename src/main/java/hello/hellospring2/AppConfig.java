package hello.hellospring2;

import hello.hellospring2.discount.DiscountPolicy;
import hello.hellospring2.discount.FixDiscountPolicy;
import hello.hellospring2.discount.RateDiscountPolicy;
import hello.hellospring2.member.MemberRepository;
import hello.hellospring2.member.MemberService;
import hello.hellospring2.member.MemberServiceImpl;
import hello.hellospring2.member.MemoryMemberRepository;
import hello.hellospring2.order.OrderService;
import hello.hellospring2.order.OrderServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class AppConfig {

    // 인젝션, 생성자 주입이라고 한다.
    // 아래와 같이 역할 속 구현이 안에 들어가게끔 리팩토링하면 좋다.
    // 이 파일의 방법은 팩토리 매서드를 통해서 등록하는 방식이라고 이야기한다.

    @Bean
    public MemberService memberService() {
        System.out.println("call AppConfig.memberService");
        return new MemberServiceImpl(memberRepository());
    }

    @Bean
    public MemberRepository memberRepository() {
        System.out.println("call AppConfig.memberRepository");
        return new MemoryMemberRepository();
    }

    @Bean
    public OrderService orderService() {
        System.out.println("call AppConfig.orderService");
        return new OrderServiceImpl(memberRepository(), discountPolicy());
    }

    @Bean
    public DiscountPolicy discountPolicy() {
//        return new FixDiscountPolicy();
        return new RateDiscountPolicy();
    }

}
